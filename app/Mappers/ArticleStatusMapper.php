<?php

namespace App\Mappers;

use App\Mappers\BaseStatusMapper;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Store;

class ArticleStatusMapper extends BaseStatusMapper
{
    const DRAFT = 1;
    const PUBLISHED = 2;
}
