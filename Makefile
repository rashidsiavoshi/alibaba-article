COMPOSE_FILES=docker-compose.yml
build:
	docker-compose -f $(COMPOSE_FILES) up -d --build

up:
	docker-compose -f $(COMPOSE_FILES) up -d
ps:
	docker-compose -f $(COMPOSE_FILES) ps
destroy:
	docker-compose -f $(COMPOSE_FILES) down

status:
	docker-compose -f $(COMPOSE_FILES) ps

shell-as-root:
	docker-compose -f $(COMPOSE_FILES) exec php sh
node:
	docker-compose -f $(COMPOSE_FILES) exec node sh
update:
	docker-compose -f $(COMPOSE_FILES) exec --user=$(USER) php sh -c 'composer update'
db:
	docker-compose -f $(COMPOSE_FILES) exec mysql mysql -u odin -p

db-root:
	docker-compose -f $(COMPOSE_FILES) exec mysql mysql -uroot -proot

redis:
	docker-compose -f $(COMPOSE_FILES) exec redis sh

log:
	docker-compose -f $(COMPOSE_FILES) logs -f --tail=100 php
