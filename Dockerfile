FROM php:8.3-fpm-alpine

WORKDIR /var/www

RUN apk add --update --no-cache \
    linux-headers \
    build-base \
    libpng-dev \
    libjpeg-turbo-dev \
    freetype-dev \
    libzip-dev \
    icu-dev \
    unzip \
        oniguruma-dev \
    curl \
    jpegoptim \
    optipng \
    pngquant \
    gifsicle \
    redis \
        libxml2-dev \
        libsodium-dev \
    libexif-dev

RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-configure zip \
     && pecl install xdebug-3.3.0 \
        && docker-php-ext-enable xdebug \
       && docker-php-ext-install pdo pdo_mysql exif \
            bcmath \
            mbstring \
            soap \
            sodium \
            zip \
            pdo \
            pdo_mysql \
            pcntl \
            exif\
            intl\
       && rm -rf /var/cache/apk/*


COPY --from=composer:2.4 /usr/bin/composer /usr/bin/composer

EXPOSE 9000

ENTRYPOINT ["./docker/entrypoint-app.sh"]
