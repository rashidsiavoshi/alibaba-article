#!/bin/sh

sleep 20
composer install
composer dump-autoload
chmod -R 777 /var/www/storage /var/www/bootstrap/cache
php artisan key:generate
php /var/www/artisan config:clear
php /var/www/artisan cache:clear
php /var/www/artisan route:clear
php /var/www/artisan view:clear
php /var/www/artisan clear-compiled
php /var/www/artisan migrate
php /var/www/artisan db:seed
php-fpm
