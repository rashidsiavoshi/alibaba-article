<?php

namespace App\Mappers;

use Illuminate\Database\Eloquent\Model;
use ReflectionClass;

abstract class BaseStatusMapper
{
    /**
     * Returns class constants
     *
     * @param bool $flip constants array key by value
     * @return array
     */
    final public static function getConstants($flip = true): array
    {
        $constants = (new ReflectionClass(static::class))->getConstants();
        return $flip ? array_flip($constants) : $constants;
    }

    /**
     * Returns class constants
     *
     * @param bool $flip
     * @return array
     */
    final public static function getConstantsValue($flip = true): array
    {
        return array_keys(self::getConstants($flip));
    }

    /**
     * Returns class constants
     *
     * @param array $values
     * @param bool $flip
     * @return array
     */
    final public static function getConstantsValues(array $values, $flip = true): array
    {
        return array_keys(array_intersect(self::getConstants($flip) , $values));
    }

    /**
     * Returns label of constant, useful for cast getter
     *
     * @param $value
     * @param bool $trans_prefix
     * @return mixed|null
     */
    final public static function getConstantLabel($value, $trans_prefix = false)
    {
        if (!isset(self::getConstants()[$value])) {
            return null;
        }

        if ($trans_prefix) {
            return [
                'display_name' => __("$trans_prefix." . self::getConstants()[$value]),
                'value' => self::getConstants()[$value],
            ];
        }
        return self::getConstants()[$value];
    }

    /**
     * Returns value of constant, useful for cast setter
     *
     * @param $name
     * @return mixed|null
     */
    final public static function getConstantValue($name)
    {
        return isset(self::getConstants()[$name]) ? $name : (self::getConstants(false)[$name] ?? null);
    }

    /**
     * Returns a random constant of mapper class
     *
     * @param bool $flip constants array key by value
     * @return mixed
     */
    final public static function getRand($flip = false)
    {
        return array_rand(self::getConstants($flip));
    }

    /**
     * Checks castable field and constant value with operator
     * @param Model $model
     * @param string $castable field name in model
     * @param string|int $constant value
     * @param string $operator
     * @return bool
     */
    final public static function compare(Model $model, string $castable, $constant, $operator = '=='): bool
    {
        $value = $model->getRawOriginal($castable);
        switch ($operator) {
            case '===':
                return $value === $constant;
            case '>=':
                return $value >= $constant;
            case '>':
                return $value > $constant;
            case '<=':
                return $value <= $constant;
            case '<':
                return $value < $constant;
            case '==':
            default:
                return $value == $constant;
        }
    }

    final public static function getConstantLabels($trans_prefix)
    {
        return collect(self::getConstants())->map(
            function ($constant) use ($trans_prefix) {
                return [
                    'display_name' => __("$trans_prefix." . $constant),
                    'value' => $constant,
                ];
            }
        )->values();
    }
}
